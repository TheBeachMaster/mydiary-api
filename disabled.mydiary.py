# -*- coding: utf-8 -*-
from flask import request, url_for, jsonify
from flask_api import FlaskAPI
import requests

app = FlaskAPI(__name__)

@app.route('/', methods=['GET'])
def home():
    return 'Home page'

#  Error Handler 404
@app.errorhandler(404)
def not_found(error=None):
    response = {
        'status' : 404,
        'message' : 'Request Not Found:' + request.url,
    }
    res = jsonify(response)
    res.status_code = 404
    return res

# Gets all projects for a particular account ID and a given Token
@app.route('/api/v1/projects/accounts/<id>', methods=['GET'])
def getAllProjects(id):
    url = "https://www.pivotaltracker.com/services/v5/projects"
    querystring = {"account_ids": str(id)}
    token_header = request.headers['X-TrackerToken']

    headers = {
        'X-TrackerToken': str(token_header),
        'Content-Type' : 'application/json'
        }
    response = requests.request("GET", url, headers=headers, params=querystring)
    return response.text

# Create a project
@app.route('/api/v1/projects/create', methods=['POST'])
def createProject():
    projectName = request.json
    url = "https://www.pivotaltracker.com/services/v5/projects"
    token_header = request.headers['X-TrackerToken']

    payload = projectName
    headers = {
        'X-TrackerToken': str(token_header),
        }
    response = requests.request("POST", url, data=payload, headers=headers)
    return response.text

# Create a story for a project : Here we pass the project ID and Token 
# The request params should be  valid jsonJSON objects
@app.route('/api/v1/projects/<id>/stories/create', methods=['POST'])
def createStory(id):
    storyDetails = request.json
    story_url = "https://www.pivotaltracker.com/services/v5/projects/"+ id +"/stories"
    token_header = request.headers['X-TrackerToken']
    headers = {
        'X-TrackerToken': str(token_header)
    }
    response = requests.request("POST", story_url, data=storyDetails, headers=headers)
    return response.text

# Get project stories :  here we pass the Proejct ID
@app.route('/api/v1/projects/<id>/stories/list', methods=['GET'])
def listProjectStories(id):
    url = "https://www.pivotaltracker.com/services/v5/projects/"+id+"/stories"
    token_header = request.headers['X-TrackerToken']
    headers = {
        'X-TrackerToken': str(token_header)
        }
    response = requests.request("GET", url, headers=headers)
    return response.text

# Edit a story  Expects a properly formated JSON object
@app.route('/api/v1/projects/<pid>/stories/<sid>/edit', methods=['PUT'])
def editStory(pid, sid):
    story_url = "https://www.pivotaltracker.com/services/v5/projects/"+ pid + "/stories/"+ sid
    edit = request.json
    token_header = request.headers['X-TrackerToken']
    headers = {
        'X-TrackerToken': str(token_header)
        }
    response = requests.request("PUT", story_url, data=edit, headers=headers)
    return response.text

if __name__ == "__main__":
    app.run(debug=True)
